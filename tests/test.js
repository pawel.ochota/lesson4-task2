const createList = require('..');
const { toBeInTheDocument, toHaveAttribute } = require('@testing-library/jest-dom/matchers');

expect.extend({ toBeInTheDocument, toHaveAttribute });

beforeEach(() => {
  document.body.innerHTML = '';
});

describe('createList', () => {
  it('should returns a HTMLElement', async () => {
    const result = createList();

    expect(result instanceof HTMLElement).toBeTruthy();
  });

  it('should returns a list with #list id attribute', async () => {
    const list = createList();

    expect(list).toHaveAttribute('id', 'list');
  });

  it('should have a list in <body> element', async () => {
    createList();

    expect(document.body.querySelector('#list')).toBeInTheDocument();
  });

  it('should have a list with 3 <li> elements', async () => {
    createList();
    const list = document.body.querySelector('#list');

    expect(list.children.length).toBe(3);
    expect(list.querySelectorAll('li').length).toBe(3);
  });
});
